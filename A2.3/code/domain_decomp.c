#include <stdio.h>

void domain_decomp(int nelems,int nproc, int rank,int* start, int* end)
{
	int quo = nelems / nproc;
	int rem = nelems % nproc;
	*start = rank*quo;
	*end = *start + quo - 1 + (rank == (nproc - 1) ? rem : 0);
}

int main(int argc, char const *argv[])
{
	int start,end;
	for (int i = 0; i < 13; ++i)
	{
		domain_decomp(54,13,i,&start,&end);
		printf("%d %d\n",start,end);
	}
	return 0;
}