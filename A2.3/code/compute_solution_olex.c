/**
 * Computational loop
 *
 * @file compute_solution.c
 * @date 22-Oct-2012
 * @author V. Petkov
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"

int compute_solution(int nprocs, int myrank, const int max_iters, int nintci, int nintcf, int nextcf, int** lcc, double* bp,
                     double* bs, double* bw, double* bl, double* bn, double* be, double* bh,
                     double* cnorm, double* var, double *su, double* cgup, double* residual_ratio,
                     int* local_global_index, int* global_local_index, int nghb_cnt, 
                     int* nghb_to_rank, int* send_cnt, int** send_lst, int *recv_cnt, int** recv_lst){
    /** parameters used in gccg */
    int iter = 1;
    int if1 = 0;
    int if2 = 0;
    int nor = 1;
    int nor1 = nor - 1;
    int nc = 0;
    int nomax = 3;

    /** the reference residual */
    double resref = 0.0;

    /** array storing residuals */
    double *resvec = (double *) calloc(sizeof(double), (nintcf + 1));

    // initialize the reference residual
    for ( nc = nintci; nc <= nintcf; nc++ ) {
        resvec[nc] = su[nc];
        resref = resref + resvec[nc] * resvec[nc];
    }

    MPI_Allreduce( MPI_IN_PLACE, &resref, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    resref = sqrt(resref);
    if ( resref < 1.0e-15 ) {
        fprintf(stderr, "Residue sum less than 1.e-15 - %lf\n", resref);
        return 0;
    }

    int local_num_ghost = 0;
    for (int i = 0; i < nghb_cnt; ++i)
    {
        local_num_ghost += recv_cnt[i];
    }

    /** the computation vectors */
    double *direc1 = (double *) calloc(sizeof(double), (nextcf + 1) + local_num_ghost);
    double *direc2 = (double *) calloc(sizeof(double), (nextcf + 1) + local_num_ghost);
    double *adxor1 = (double *) calloc(sizeof(double), (nintcf + 1));
    double *adxor2 = (double *) calloc(sizeof(double), (nintcf + 1));
    double *dxor1 = (double *) calloc(sizeof(double), (nintcf + 1));
    double *dxor2 = (double *) calloc(sizeof(double), (nintcf + 1));

    MPI_Request* requests = (MPI_Request *) calloc(sizeof(MPI_Request), 2 * nghb_cnt);
    for (int i = 0; i < 2 * nghb_cnt; i++)
    {
        requests[i] = MPI_REQUEST_NULL;
    }

    MPI_Status* statuses = (MPI_Status *) calloc(sizeof(MPI_Status), 2 * nghb_cnt);

    MPI_Datatype *send_types = (MPI_Datatype *) calloc(sizeof(MPI_Datatype), nghb_cnt);
    int** send_blocks = (int **) calloc(sizeof(int*), nghb_cnt);
    int** send_displs = (int **) calloc(sizeof(int*), nghb_cnt);

    MPI_Datatype *recv_types = (MPI_Datatype *) calloc(sizeof(MPI_Datatype), nghb_cnt);
    int** recv_blocks = (int **) calloc(sizeof(int*), nghb_cnt);
    int** recv_displs = (int **) calloc(sizeof(int*), nghb_cnt);

    int offset = 0;
    for(int i = 0; i < nghb_cnt; i++)
    {
        send_blocks[i] = (int *) calloc(sizeof(int), send_cnt[i]);
        send_displs[i] = (int *) calloc(sizeof(int), send_cnt[i]);
        
        for(int j = 0; j < send_cnt[i]; j++)
        {
            send_blocks[i][j] = 1;
            send_displs[i][j] = global_local_index[send_lst[i][j]];
        }

        MPI_Type_indexed(send_cnt[i], send_blocks[i], send_displs[i], MPI_DOUBLE, send_types + i);
        MPI_Type_commit(send_types + i);

                
        recv_blocks[i] = (int *) calloc(sizeof(int), 1);
        recv_displs[i] = (int *) calloc(sizeof(int), 1);

        recv_blocks[i][0] = recv_cnt[i];
        recv_displs[i][0] = nextcf + 1 + offset;
        offset += recv_cnt[i];


        MPI_Type_indexed(1, recv_blocks[i], recv_displs[i], MPI_DOUBLE, recv_types + i);
        MPI_Type_commit(recv_types + i);
    }

    while ( iter < max_iters ) {
        /**********  START COMP PHASE 1 **********/
        // update the old values of direc
        for ( nc = nintci; nc <= nintcf; nc++ ) {
            direc1[nc] = direc1[nc] + resvec[nc] * cgup[nc];
        }

        for (int i = 0; i < nghb_cnt; i++)
        {
            MPI_Isend(direc1, 1, send_types[i], nghb_to_rank[i], myrank, MPI_COMM_WORLD, requests + i);
        }
        for (int i = 0; i < nghb_cnt; i++)
        {
            MPI_Irecv(direc1, 1, recv_types[i], nghb_to_rank[i], nghb_to_rank[i], MPI_COMM_WORLD, requests + nghb_cnt + i);
        }
        MPI_Waitall(nghb_cnt * 2, requests, statuses);
        
        // compute new guess (approximation) for direc
        for ( nc = nintci; nc <= nintcf; nc++ ) {
            direc2[nc] = bp[nc] * direc1[nc] - bs[nc] * direc1[global_local_index[lcc[nc][0]]]
                         - be[nc] * direc1[global_local_index[lcc[nc][1]]] - bn[nc] * direc1[global_local_index[lcc[nc][2]]]
                         - bw[nc] * direc1[global_local_index[lcc[nc][3]]] - bl[nc] * direc1[global_local_index[lcc[nc][4]]]
                         - bh[nc] * direc1[global_local_index[lcc[nc][5]]];
        }
        /********** END COMP PHASE 1 **********/

        /********** START COMP PHASE 2 **********/
        // execute normalization steps
        double oc1, oc2, occ;
        if ( nor1 == 1 ) {
            oc1 = 0;
            occ = 0;

            for ( nc = nintci; nc <= nintcf; nc++ ) {
                occ = occ + direc2[nc] * adxor1[nc];
            }

            MPI_Allreduce( MPI_IN_PLACE, &occ, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

            oc1 = occ / cnorm[1];
            for ( nc = nintci; nc <= nintcf; nc++ ) {
                direc2[nc] = direc2[nc] - oc1 * adxor1[nc];
                direc1[nc] = direc1[nc] - oc1 * dxor1[nc];
            }

            if1++;
        } else {
            if ( nor1 == 2 ) {
                oc1 = 0;
                occ = 0;

                for ( nc = nintci; nc <= nintcf; nc++ ) {
                    occ = occ + direc2[nc] * adxor1[nc];
                }

                MPI_Allreduce( MPI_IN_PLACE, &occ, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

                oc1 = occ / cnorm[1];
                oc2 = 0;
                occ = 0;
                for ( nc = nintci; nc <= nintcf; nc++ ) {
                    occ = occ + direc2[nc] * adxor2[nc];
                }

                MPI_Allreduce( MPI_IN_PLACE, &occ, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

                oc2 = occ / cnorm[2];
                for ( nc = nintci; nc <= nintcf; nc++ ) {
                    direc1[nc] = direc1[nc] - oc1 * dxor1[nc] - oc2 * dxor2[nc];
                    direc2[nc] = direc2[nc] - oc1 * adxor1[nc] - oc2 * adxor2[nc];
                }

                if2++;
            }
        }

        // compute the new residual
        cnorm[nor] = 0;
        double omega = 0;
        for ( nc = nintci; nc <= nintcf; nc++ ) {
            cnorm[nor] = cnorm[nor] + direc2[nc] * direc2[nc];
            omega = omega + resvec[nc] * direc2[nc];
        }

        MPI_Allreduce( MPI_IN_PLACE, &(cnorm[nor]), 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Allreduce( MPI_IN_PLACE, &omega, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);


        omega = omega / cnorm[nor];
        double res_updated = 0.0;
        for ( nc = nintci; nc <= nintcf; nc++ ) {
            resvec[nc] = resvec[nc] - omega * direc2[nc];
            res_updated = res_updated + resvec[nc] * resvec[nc];
            var[nc] = var[nc] + omega * direc1[nc];
        }


        MPI_Allreduce( MPI_IN_PLACE, &res_updated, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        res_updated = sqrt(res_updated);
        *residual_ratio = res_updated / resref;

        // exit on no improvements of residual
        if ( *residual_ratio <= 1.0e-10 ) break;

        iter++;

        // prepare additional arrays for the next iteration step
        if ( nor == nomax ) {
            nor = 1;
        } else {
            if ( nor == 1 ) {
                for ( nc = nintci; nc <= nintcf; nc++ ) {
                    dxor1[nc] = direc1[nc];
                    adxor1[nc] = direc2[nc];
                }
            } else {
                if ( nor == 2 ) {
                    for ( nc = nintci; nc <= nintcf; nc++ ) {
                        dxor2[nc] = direc1[nc];
                        adxor2[nc] = direc2[nc];
                    }
                }
            }

            nor++;
        }
        nor1 = nor - 1;
        /********** END COMP PHASE 2 **********/
    }

    //free the supplementary MPI bullshit
    free(requests);
    free(statuses);
    free(send_types);
    free(recv_types);

    for (int i = 0; i < nghb_cnt; ++i)
    {
        free(send_blocks[i]);
        free(send_displs[i]);
        free(recv_blocks[i]);
        free(recv_displs[i]);
    }

    free(send_blocks);
    free(send_displs);
    free(recv_blocks);
    free(recv_displs);   

    //free the additional arrays
    free(direc1);
    free(direc2);
    free(adxor1);
    free(adxor2);
    free(dxor1);
    free(dxor2);
    free(resvec);

    return iter;
}


