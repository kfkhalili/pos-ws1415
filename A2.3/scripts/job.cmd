#@ job_name = pos_ag
#@ job_type = parallel
#@ output = job.out
#@ error = job.err
#@ class = test
#@ node = 1
#@ total_tasks = 16
#@ energy_policy_tag = NONE
#@ island_count = 1
#@ wall_clock_limit = 01:00:00
#@ notification = always
#@ notify_user = kfkhalili@hotmail.com
#@ queue
mpiexec -n 16 ./gccg drall.geo.bin classic allread
echo "JOB is run"
