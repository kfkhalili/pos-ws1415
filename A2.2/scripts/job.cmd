#@ job_name = pos_ag
#@ job_type = parallel
#@ output = job.out
#@ error = job.err
#@ class = test
#@ node = 1
#@ total_tasks = 9
#@ energy_policy_tag = NONE
#@ island_count = 1
#@ wall_clock_limit = 01:00:00
#@ notification = always
#@ notify_user = kfkhalili@hotmail.com
#@ queue
mpiexec -n 9 ./gccg drall.geo.bin classic allread
mpiexec -n 9 ./gccg drall.geo.bin dual oneread
mpiexec -n 9 ./gccg cojack.geo.bin classic allread
mpiexec -n 9 ./gccg cojack.geo.bin dual oneread
echo "JOB is run"
