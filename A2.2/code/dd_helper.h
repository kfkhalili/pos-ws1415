#ifndef DD_HELPER_H_
#define DD_HELPER_H_

#include "metis.h"

void classic_decomp(int nelems,int nproc, int rank,int* start, int* end);

void gen_eptr(int nelems, idx_t *eptr, int nPoinsElems);

void copy_elems_metis(int nelems, idx_t *eind, int *elems);

void reorg_part_metis(int nelems,int nproc, idx_t *arr,int *oArr, int *nElemsProc, int *map);

void get_displacements(int nproc, int *numElemsProc, int *disp);

void reorder_withmap(int nelems,double *destination, double *source, int *map);

void reorder_withmap_disp(int nelems,double *destination, double *source, int *map, int disp);

void reorder_lcc_withmap_disp(int nelems, int **destination, int **source, int *map, int disp);

void flatten_lcc(int nelems, int **lcc, int *lcc_flat);

void deflatten_lcc(int nelems, int **lcc, int *lcc_flat);

void count_extCels(int nelems,int nlocal, int **lcc, int *nExtElems);

void copy_lcc(int nelems, int **lcc_old, int **lcc_new);

void critical_print(int count, int myrank, int nprocs, int **array);

int count_unique_ints(int nelems, int *arr);

int already_in(int val, int nelems, int *arr);

int get_ngh_idx(int val,int nelems, int *ntr);

#endif