#@ job_name = pos_2procs
#@ job_type = parallel
#@ output = output/job(jobid).out
#@ error = error/job(jobid).err
#@ class = test
#@ node = 1
#@ total_tasks = 8
#@ energy_policy_tag = NONE
#@ island_count = 1
#@ wall_clock_limit = 01:00:00
#@ notification = always
#@ notify_user = kfkhalili@hotmail.com
#@ queue

export MP_TASK_AFFINITY=cpu
export SCOREP_ENABLE_TRACING="true"
export SCOREP_ENABLE_PROFILING="true"

for input in "pent.geo.bin" "cojack.geo.bin" "drall.geo.bin"
do
    for partition in "dual" "classic" "nodal"
    do
        for ptype in "oneread" "allread"
        do
                export SCOREP_EXPERIMENT_DIRECTORY="scorep/$input/$partition/$ptype/08"
                mpiexec -n 8 ./gccg $input $partition $ptype
        done
    done
done

echo JOBS ARE RUN
