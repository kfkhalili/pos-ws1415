#@ job_name = pos_1proc
#@ job_type = parallel
#@ output = output/job$(jobid).out
#@ error = error/job$(jobid).err
#@ class = test
#@ node = 4
#@ total_tasks = 64
#@ energy_policy_tag = NONE
#@ island_count = 1
#@ wall_clock_limit = 01:00:00
#@ notification = always
#@ notify_user = kfkhalili@hotmail.com
#@ queue

export MP_TASK_AFFINITY=cpu

for input in  "drall.geo.bin" "cojack.geo.bin"
do
   for partition in "dual"
   do
      for ptype in "oneread" "allread"
      do
         for iteration in "it1" "it2" "it3"
         do
                mpiexec -n 64 ./gccg $input $partition $ptype
         done
      done
   done
done

echo JOBS ARE RUN

