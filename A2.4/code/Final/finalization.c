/**
 * Finalization step - write results and other computational vectors to files
 *
 * @date 22-Oct-2012
 * @author V. Petkov
 */

#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include "util_write_files.h"

void finalization(char *file_in, int nprocs, int myrank, int total_iters, double residual_ratio,
                  int nintci, int nintcf, double *var, int *local_global_index, int *global_local_index)
{
	// if there is more than one process then we need to write global maxs and mins
    if (nprocs > 1)
    {
        int **universal_lgmap;
        double **universal_var;
        double *ultimate_var;
        MPI_Status *status;
        MPI_Request *request;

        request = ( MPI_Request *) calloc(sizeof(MPI_Request), 2 * nprocs);
        for (int i = 0; i < 2 * nprocs; ++i)
        {
            request[i] = MPI_REQUEST_NULL;
        }
        status = (MPI_Status *) calloc(sizeof(MPI_Status), 2 * nprocs);

        int nintcfarr[nprocs];
        //gather all the local nintcf indexes
        MPI_Gather(&nintcf, 1, MPI_INT, nintcfarr, 1, MPI_INT, 0, MPI_COMM_WORLD);
        // send the local global and var arrays to process 0
        MPI_Isend(local_global_index, nintcf + 1, MPI_INT, 0, myrank, MPI_COMM_WORLD, &(request[myrank]));
        MPI_Isend(var, nintcf + 1, MPI_DOUBLE, 0, myrank + nprocs, MPI_COMM_WORLD, &(request[myrank + nprocs]));

        // just process 0 will write the stats file
        if (myrank == 0)
        {
            universal_lgmap = (int **) calloc(sizeof(int *), nprocs);
            universal_var = (double **) calloc(sizeof(double *), nprocs);
            // generating arrays with var and local global arrays from the other processes
            int universal_nintcf = nprocs - 1;
            for (int i = 0; i < nprocs; ++i)
            {
                universal_var[i] = (double *) calloc(sizeof(double), nintcfarr[i] + 1);
                universal_lgmap[i] = (int *) calloc(sizeof(int), nintcfarr[i] + 1);
                MPI_Recv(universal_lgmap[i], nintcfarr[i] + 1, MPI_INT, i, i, MPI_COMM_WORLD, &(status[i]));
                MPI_Recv(universal_var[i], nintcfarr[i] + 1, MPI_DOUBLE, i, i + nprocs, MPI_COMM_WORLD, &(status[i + nprocs]));
                universal_nintcf += nintcfarr[i];
            }


            ultimate_var = (double *) calloc(sizeof(double), universal_nintcf + 1);

            // creating one single var array for writing the stats
            for (int i = 0; i < nprocs; ++i)
            {
                for (int j = 0; j <= nintcfarr[i]; ++j)
                {
                    ultimate_var[universal_lgmap[i][j]] = universal_var[i][j];
                }
            }

            char file_out[100];
            sprintf(file_out, "%s_summary.out", file_in);
            //writing the stats with the values from all the processes to have a global stats file
            int final_status = store_simulation_stats(file_in, file_out, nintci, universal_nintcf, ultimate_var, total_iters, residual_ratio);

            if ( final_status != 0 ) fprintf(stderr, "Error when trying to write to file %s\n", file_out);

            for (int i = 0; i < nprocs; ++i)
            {
                free(universal_lgmap[i]);
                free(universal_var[i]);
            }
            free(universal_lgmap);
            free(universal_var);
            free(ultimate_var);
        }
        free(status);
        free(request);
    }
    else //if there us only one process then write stats normally.
    {
        char file_out[100];
        sprintf(file_out, "%s_summary.out", file_in);

        int status = store_simulation_stats(file_in, file_out, nintci, nintcf, var, total_iters,
                                            residual_ratio);

        if ( status != 0 ) fprintf(stderr, "Error when trying to write to file %s\n", file_out);
    }
}

