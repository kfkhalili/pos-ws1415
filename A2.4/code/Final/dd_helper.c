#include "metis.h"
#include "mpi.h"
#include <stdio.h>
void classic_decomp(int nelems, int nproc, int rank, int *start, int *end)
{
    int quo = nelems / nproc;
    int rem = nelems % nproc;
    *start = rank * quo;
    *end = *start + quo - 1 + (rank == (nproc - 1) ? rem : 0);
}
// nelems = num of total elements, *eptr = array of indexes of elems, nPoinsElems = num nodes per elem
void gen_eptr(int nelems, idx_t *eptr, int nPoinsElems)
{
    for (int i = 0; i <= nelems; ++i)
    {
        eptr[i] = i * nPoinsElems;
    }
}

//copy elements to the weird metis array format
void copy_elems_metis(int nelems, idx_t *eind, int *elems)
{
    for (int i = 0; i < nelems * 8; ++i)
    {
        eind[i] = elems[i];
    }
}

//Reorganize and get number of elems per process
void reorg_part_metis_idx(int nelems, int nproc, idx_t *arr, int *nElemsProc, int *map)
{
    int counter = 0;
    for (int i = 0; i < nproc; ++i)
    {
        for (int j = 0; j < nelems; ++j)
        {
            if (arr[j] == i)
            {
                map[counter] = j;
                nElemsProc[i]++;
                counter++;
            }
        }
    }
}
//Reorganize and get number of elems per process with an int array
void reorg_part_metis(int nelems, int nproc, int *arr, int *nElemsProc, int *map)
{
    int counter = 0;
    for (int i = 0; i < nproc; ++i)
    {
        for (int j = 0; j < nelems; ++j)
        {
            if (arr[j] == i)
            {
                map[counter] = j;
                nElemsProc[i]++;
                counter++;
            }
        }
    }
}

// calculate the displacement for MPI_Scatterv 
void get_displacements(int nproc, int *numElemsProc, int *disp)
{
    disp[0] = 0;
    for (int i = 1; i < nproc; ++i)
    {
        disp[i] = disp[i - 1] + numElemsProc[i - 1];
    }
}

// reorders the arrays according to our map so the elements corresponding to each process are together.
void reorder_withmap(int nelems, double *destination, double *source, int *map)
{
    for (int i = 0; i < nelems; ++i)
    {
        destination[i] = source[map[i]];
    }
}

// reorders the arrays according to a part our map using the displacements so the elements corresponding to each process are together.
void reorder_withmap_disp(int nelems, double *destination, double *source, int *map, int disp)
{
    for (int i = 0; i < nelems; ++i)
    {
        destination[i] = source[map[i + disp]];
    }
}
// reorders lcc according to a part our map using the displacements.
void reorder_lcc_withmap_disp(int nelems, int **destination, int **source, int *map, int disp)
{
    for (int i = 0; i < nelems; ++i)
    {
        destination[i][0] = source[map[i + disp]][0];
        destination[i][1] = source[map[i + disp]][1];
        destination[i][2] = source[map[i + disp]][2];
        destination[i][3] = source[map[i + disp]][3];
        destination[i][4] = source[map[i + disp]][4];
        destination[i][5] = source[map[i + disp]][5];
    }
}

// copying lcc into a flat array so we can send it easily with MPI
void flatten_lcc(int nelems, int **lcc, int *lcc_flat)
{
    int k = 0;
    for (int i = 0; i < nelems; i++)
    {
        lcc_flat[k] = lcc[i][0];
        lcc_flat[k + 1] = lcc[i][1];
        lcc_flat[k + 2] = lcc[i][2];
        lcc_flat[k + 3] = lcc[i][3];
        lcc_flat[k + 4] = lcc[i][4];
        lcc_flat[k + 5] = lcc[i][5];
        k += 6;
    }
}
//reverts the flattening operation that we performed previously
void deflatten_lcc(int nelems, int **lcc, int *lcc_flat)
{
    int k = 0;
    for (int i = 0; i < nelems; i++)
    {
        lcc[i][0] = lcc_flat[k];
        lcc[i][1] = lcc_flat[k + 1];
        lcc[i][2] = lcc_flat[k + 2];
        lcc[i][3] = lcc_flat[k + 3];
        lcc[i][4] = lcc_flat[k + 4];
        lcc[i][5] = lcc_flat[k + 5];
        k += 6;
    }
}
// calculates the amount of external cells in the local lcc and writes the count into an array
void count_extCels(int nelems, int nlocal, int **lcc, int *nExtElems)
{
    *nExtElems = 0;
    for (int i = 0; i < nlocal; ++i)
    {
        for (int j = 0; j < 6; ++j)
        {
            if (lcc[i][j] >= nelems)
            {
                *nExtElems += 1;
            }
        }
    }
}

// we use this function to copy the local lcc array to the normal lcc
void copy_lcc(int nelems, int **lcc_old, int **lcc_new)
{
    for (int i = 0; i < nelems; ++i)
    {
        lcc_new[i][0] = lcc_old[i][0];
        lcc_new[i][1] = lcc_old[i][1];
        lcc_new[i][2] = lcc_old[i][2];
        lcc_new[i][3] = lcc_old[i][3];
        lcc_new[i][4] = lcc_old[i][4];
        lcc_new[i][5] = lcc_old[i][5];
    }
}

// print function for testing purposes
void critical_print(int count, int myrank, int nprocs, int **array)
{
    int rank = 0;
    while (rank < nprocs)
    {
        if (myrank == rank)
        {
            for (int i = 0; i < count; ++i)
            {
                printf("Rank %d: Array[%d]: %d\n", myrank, i, (*array)[i]);
            }
            fflush (stdout);
        }
        rank ++;
        MPI_Barrier(MPI_COMM_WORLD);
    }
}

// count unique integers in an array
int count_unique_ints(int nelems, int *arr)
{
    int temp[nelems];
    int count = 1;
    temp[0] = arr[0];
    int k;
    for (int i = 1; i < nelems; ++i)
    {
        k = 0;
        while (arr[i] != temp[k])
        {
            if (i == k)
            {
                temp[count] = arr[i];
                count++;
            }
            k++;
        }

    }
    return count;
}

// verifies that an element exists in an array
int already_in(int val, int nelems, int *arr)
{
    for (int i = 0; i < nelems; ++i)
    {
        if (arr[i] == val)
        {
            return 1;
        }
    }
    return 0;
}


// returns the index of a particular value in an array
int get_ngh_idx(int val,int nelems, int *ntr)
{
    for (int i = 0; i < nelems; ++i)
    {
        if (ntr[i] == val)
        {
            return i;
        }
    }
    return -1;
}

// used for creating our MPI indexed type, and gets the number of different blocks that we will send
void get_mpi_blocks_cnt(int sor_cnt, int *sor_lst,int *gl_map, int *blocks_cnt)
{
    *blocks_cnt = 1;

    for (int i = 1; i < sor_cnt; ++i)
    {
        if (!(gl_map[sor_lst[i]] - gl_map[sor_lst[i-1]] == 1))
        {
            *blocks_cnt += 1;
        }
    }

}  

// used for creating our MPI indexed type, and gets the lenght of each block and the displacements of them
void get_mpi_blocks_len_disps(int sor_cnt, int *sor_lst,int *gl_map, int *blocks_length, int *disps)
{
    int cnt = 0;


    for (int i = 1; i < sor_cnt; ++i)
    {
        if (!(gl_map[sor_lst[i]] == gl_map[sor_lst[i-1]] + 1))
        {
            disps[cnt] = gl_map[sor_lst[i-1]] - blocks_length[cnt] ;
            blocks_length[cnt] += 1;
            cnt += 1;
        }
        else
        {
            blocks_length[cnt] +=1;
        }
    }
    disps[cnt] = gl_map[sor_lst[sor_cnt-1]] - blocks_length[cnt];
    blocks_length[cnt] +=1;
    
}  


