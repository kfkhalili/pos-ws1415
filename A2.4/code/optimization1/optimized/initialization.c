/**
 * Initialization step - parse the input file, compute data distribution, initialize LOCAL computational arrays
 *
 * @date 22-Oct-2012, 03-Nov-2014
 * @author V. Petkov, A. Berariu
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "util_read_files.h"
#include "initialization.h"
#include "test_functions.h"
#include "mpi.h"
#include "dd_helper.h"
#include "metis.h"
//#include "papi.h"

void handle_error()
{
    printf("there was an error with PAPI\n");
    exit(1);
}

int cmpfunc (const void * a, const void * b)
{
   return ( *(int*)a - *(int*)b );
}

int initialization(char *file_in, char *part_type, char *read_type, int nprocs, int myrank,
                   int *nintci, int *nintcf, int *nextci,
                   int *nextcf, int *** lcc, double **bs, double **be, double **bn, double **bw,
                   double **bl, double **bh, double **bp, double **su, int *points_count,
                   int *** points, int **elems, double **var, double **cgup, double **oc,
                   double **cnorm, int **local_global_index, int **global_local_index,
                   int *nghb_cnt, int **nghb_to_rank, int **send_cnt, int *** send_lst,
                   int **recv_cnt, int *** recv_lst)
{
    /********** START INITIALIZATION **********/

    // papi not supported on local machine

    /*    long long start_vusec, end_vusec, final_vusec;

        if ( PAPI_library_init( PAPI_VER_CURRENT ) != PAPI_VER_CURRENT ) exit(1);

        start_vusec = PAPI_get_virt_usec();
    */


    // read-in the input file
    int f_status = 0;

    int num_tot_elems, tot_exts;


    // idx_t intialization
    idx_t ne, nn, nparts, ncommon, *objval;
    idx_t *eptr;
    idx_t *eind;
    idx_t *epart;
    idx_t *npart;

    int *nElemsProc, *epart_int, *nExtElemsProc, *nNodesProc, *oEpart, *eMap, *nMap, *oNpart, *disps, *nElemsProc_lcc, *disps_lcc, *lcc_flat, **lcc_new;
    double *bs_new, *be_new, *bw_new, *bn_new, *bl_new, *bh_new, *bp_new, *su_new;

    nElemsProc = (int *) calloc(sizeof(int), nprocs);
    nExtElemsProc = (int *) calloc(sizeof(int), nprocs);
    disps = (int *) calloc(sizeof(int), nprocs);


    if (myrank == 0)
    {
        f_status  = read_binary_geo(file_in, &*nintci, &*nintcf, &*nextci, &*nextcf, &*lcc, &*bs,
                                    &*be, &*bn, &*bw, &*bl, &*bh, &*bp, &*su, &*points_count,
                                    &*points, &*elems);

        num_tot_elems  = *nintcf + 1;
        tot_exts = *nextcf - *nextci + 1;

        eMap = (int *) calloc(sizeof(int), num_tot_elems);
        nMap = (int *) calloc(sizeof(int), *points_count);

        if (strcmp(part_type, "classic") == 0)
        {
            int start = 0;
            int end = 0 ;
            epart_int = (int *) calloc(sizeof(int), num_tot_elems);


            for (int i = 0; i < nprocs; ++i)
            {
                classic_decomp(num_tot_elems, nprocs, i, &start, &end);
                for (int j = start; j <= end; ++j)
                {
                    epart_int[j] = i;
                    nElemsProc[i] += 1;
                    eMap[j] = j;
                }
            }

        }

        if (strcmp(part_type, "dual") == 0 || strcmp(part_type, "nodal") == 0)
        {
            ne = num_tot_elems;
            nn = *points_count;
            ncommon = 4;
            nparts = nprocs;

            eptr = (idx_t *) calloc(sizeof(idx_t), (ne + 1));
            eind = (idx_t *) calloc(sizeof(idx_t), (ne * 8));
            epart = (idx_t *) calloc(sizeof(idx_t), ne);
            npart = (idx_t *) calloc(sizeof(idx_t), nn);
            objval = (idx_t *) calloc(sizeof(idx_t), 1);

            oEpart = (int *) calloc(sizeof(int), num_tot_elems);
            oNpart = (int *) calloc(sizeof(int), *points_count);
            nNodesProc = (int *) calloc(sizeof(int), nprocs);
            eMap = (int *) calloc(sizeof(int), num_tot_elems);
            nMap = (int *) calloc(sizeof(int), *points_count);


            gen_eptr(num_tot_elems, eptr, 8);
            copy_elems_metis(num_tot_elems, eind, *elems);

            if (strcmp(part_type, "dual") == 0)
            {

                if (METIS_PartMeshDual(&ne, &nn, eptr, eind, NULL, NULL, &ncommon, &nparts, NULL, NULL, objval, epart, npart) != METIS_OK) printf("Error with METIS_PartMeshDual\n");

            }
            if (strcmp(part_type, "nodal") == 0)
            {

                if (METIS_PartMeshNodal(&ne, &nn, eptr, eind, NULL, NULL, &nparts, NULL, NULL, objval, epart, npart) != METIS_OK) printf("Error with METIS_PartMeshNodall\n");

            }

            reorg_part_metis(num_tot_elems, nprocs, epart, oEpart, nElemsProc, eMap);
            reorg_part_metis(*points_count, nprocs, npart, oNpart, nNodesProc, nMap);


        }

        get_displacements(nprocs, nElemsProc, disps);


        //needed for oneread to send the correct elements.
        bs_new = (double *) calloc(sizeof(double), *nextcf + 1);
        bn_new = (double *) calloc(sizeof(double), *nextcf + 1);
        bw_new = (double *) calloc(sizeof(double), *nextcf + 1);
        bl_new = (double *) calloc(sizeof(double), *nextcf + 1);
        bh_new = (double *) calloc(sizeof(double), *nextcf + 1);
        be_new = (double *) calloc(sizeof(double), *nextcf + 1);
        bp_new = (double *) calloc(sizeof(double), *nextcf + 1);
        su_new = (double *) calloc(sizeof(double), *nextcf + 1);

        lcc_new = (int **) malloc((*nintcf + 1) * sizeof(int *));
        for (int i = 0; i < *nintcf + 1; i++ )
        {
            lcc_new[i] = (int *) malloc(6 * sizeof(int));
        }

        reorder_withmap(num_tot_elems, bs_new, *bs, eMap);
        reorder_withmap(num_tot_elems, bn_new, *bn, eMap);
        reorder_withmap(num_tot_elems, bw_new, *bw, eMap);
        reorder_withmap(num_tot_elems, bl_new, *bl, eMap);
        reorder_withmap(num_tot_elems, bh_new, *bh, eMap);
        reorder_withmap(num_tot_elems, be_new, *be, eMap);
        reorder_withmap(num_tot_elems, bp_new, *bp, eMap);
        reorder_withmap(num_tot_elems, su_new, *su, eMap);

        reorder_lcc_withmap_disp(num_tot_elems, lcc_new, *lcc, eMap, 0);

        lcc_flat = (int *) calloc(sizeof(int), num_tot_elems * 6);
        flatten_lcc(num_tot_elems, lcc_new, lcc_flat);


        if (strcmp(part_type, "classic") != 0)
        {
            epart_int = (int *) calloc(num_tot_elems, sizeof(int));
            for (int i = 0; i < num_tot_elems; ++i)
            {
                epart_int[i] = epart[i];
            }
        }

    }

    double *bs_local, *be_local, *bw_local, *bn_local, *bl_local, *bh_local, *bp_local, *su_local;
    int *lcc_local_flat, **lcc_local;
    //reading strategies start here....
    if (strcmp(read_type, "oneread") == 0)
    {


        MPI_Bcast( nElemsProc, nprocs, MPI_INT, 0, MPI_COMM_WORLD );
        MPI_Bcast( disps, nprocs, MPI_INT, 0, MPI_COMM_WORLD );
        MPI_Bcast( &num_tot_elems, 1, MPI_INT, 0, MPI_COMM_WORLD );
        MPI_Bcast( points_count, 1, MPI_INT, 0, MPI_COMM_WORLD );



        *nintci = 0;
        *nintcf = nElemsProc[myrank] - 1;
        *nextci = nElemsProc[myrank];


        if (myrank != 0)
        {
            *points = (int **) calloc(*points_count, sizeof(int *));
            for (int i = 0; i < *points_count; ++i)
            {
                (*points)[i] = (int *) calloc(3, sizeof(int));
            }
            *elems = (int *) malloc(sizeof(int) * (*nintcf + 1) * 8 );
            eMap = (int *) calloc(sizeof(int), num_tot_elems);
            epart_int = (int *) calloc(sizeof(int), num_tot_elems);
        }

        MPI_Bcast( eMap, num_tot_elems, MPI_INT, 0, MPI_COMM_WORLD );

        if (myrank == 0)
        {
            free(*su);
            free(*bp);
            free(*bh);
            free(*bl);
            free(*bw);
            free(*bn);
            free(*be);
            free(*bs);

            for (int i = 0; i < num_tot_elems; i++ )
            {
                free((*lcc)[i]);
            }
            free(*lcc);
        }

        *lcc = (int **) malloc((*nintcf + 1) * sizeof(int *));
        for (int i = 0; i < *nintcf + 1; i++ )
        {
            (*lcc)[i] = (int *) malloc(6 * sizeof(int));
        }


        lcc_local_flat = (int *) calloc(sizeof(int), nElemsProc[myrank] * 6);


        nElemsProc_lcc = (int *) calloc(sizeof(int), nprocs);
        disps_lcc = (int *) calloc(sizeof(int), nprocs);
        for (int i = 0; i < nprocs; ++i)
        {
            nElemsProc_lcc[i] = nElemsProc[i] * 6;
            disps_lcc[i] = disps[i] * 6;

        }
        MPI_Scatterv(lcc_flat, nElemsProc_lcc, disps_lcc, MPI_INT, lcc_local_flat, nElemsProc_lcc[myrank], MPI_INT, 0, MPI_COMM_WORLD);
        deflatten_lcc(nElemsProc[myrank], *lcc, lcc_local_flat);
        count_extCels(num_tot_elems, nElemsProc[myrank], *lcc, &nExtElemsProc[myrank]);
        *nextcf = nElemsProc[myrank] + nExtElemsProc[myrank] - 1;

        *bs = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        *bn = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        *bw = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        *bl = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        *bh = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        *be = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        *bp = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        *su = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);


        MPI_Scatterv(bs_new, nElemsProc, disps, MPI_DOUBLE, *bs, nElemsProc[myrank], MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Scatterv(be_new, nElemsProc, disps, MPI_DOUBLE, *be, nElemsProc[myrank], MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Scatterv(bn_new, nElemsProc, disps, MPI_DOUBLE, *bn, nElemsProc[myrank], MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Scatterv(bw_new, nElemsProc, disps, MPI_DOUBLE, *bw, nElemsProc[myrank], MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Scatterv(bl_new, nElemsProc, disps, MPI_DOUBLE, *bl, nElemsProc[myrank], MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Scatterv(bh_new, nElemsProc, disps, MPI_DOUBLE, *bh, nElemsProc[myrank], MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Scatterv(bp_new, nElemsProc, disps, MPI_DOUBLE, *bp, nElemsProc[myrank], MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Scatterv(su_new, nElemsProc, disps, MPI_DOUBLE, *su, nElemsProc[myrank], MPI_DOUBLE, 0, MPI_COMM_WORLD);

        *local_global_index = (int *) calloc(sizeof(int), nElemsProc[myrank]);

        for (int i = 0; i < nElemsProc[myrank]; ++i)
        {
            (*local_global_index)[i] = eMap[disps[myrank] + i];
        }


        free(lcc_local_flat);
    }

    if (strcmp(read_type, "allread") == 0)
    {

        if (myrank != 0)
        {
            f_status  = read_binary_geo(file_in, &*nintci, &*nintcf, &*nextci, &*nextcf, &*lcc, &*bs,
                                        &*be, &*bn, &*bw, &*bl, &*bh, &*bp, &*su, &*points_count,
                                        &*points, &*elems);

            num_tot_elems  = *nintcf + 1;
            eMap = (int *) calloc(sizeof(int), num_tot_elems);
            epart_int = (int *) calloc(sizeof(int), num_tot_elems);
        }


        MPI_Bcast( nElemsProc, nprocs, MPI_INT, 0, MPI_COMM_WORLD );
        MPI_Bcast( eMap, num_tot_elems, MPI_INT, 0, MPI_COMM_WORLD );
        MPI_Bcast( disps, nprocs, MPI_INT, 0, MPI_COMM_WORLD );


        *nintci = 0;
        *nintcf = nElemsProc[myrank] - 1;
        *nextci = nElemsProc[myrank];


        lcc_local = (int **) malloc((*nintcf + 1) * sizeof(int *));
        for (int i = 0; i < *nintcf + 1; i++ )
        {
            lcc_local[i] = (int *) malloc(6 * sizeof(int));
        }

        reorder_lcc_withmap_disp(nElemsProc[myrank], lcc_local, *lcc, eMap, disps[myrank]);
        count_extCels(num_tot_elems, nElemsProc[myrank], lcc_local, &nExtElemsProc[myrank]);
        *nextcf = nElemsProc[myrank] + nExtElemsProc[myrank] - 1;

        bs_local = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        bn_local = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        bw_local = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        bl_local = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        bh_local = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        be_local = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        bp_local = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        su_local = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);


        reorder_withmap_disp(nElemsProc[myrank], bs_local, *bs, eMap, disps[myrank]);
        reorder_withmap_disp(nElemsProc[myrank], bn_local, *bn, eMap, disps[myrank]);
        reorder_withmap_disp(nElemsProc[myrank], bw_local, *bw, eMap, disps[myrank]);
        reorder_withmap_disp(nElemsProc[myrank], bl_local, *bl, eMap, disps[myrank]);
        reorder_withmap_disp(nElemsProc[myrank], bh_local, *bh, eMap, disps[myrank]);
        reorder_withmap_disp(nElemsProc[myrank], be_local, *be, eMap, disps[myrank]);
        reorder_withmap_disp(nElemsProc[myrank], bp_local, *bp, eMap, disps[myrank]);
        reorder_withmap_disp(nElemsProc[myrank], su_local, *su, eMap, disps[myrank]);

        nElemsProc_lcc = (int *) calloc(sizeof(int), nprocs);
        disps_lcc = (int *) calloc(sizeof(int), nprocs);
        for (int i = 0; i < nprocs; ++i)
        {
            nElemsProc_lcc[i] = nElemsProc[i] * 6;
            disps_lcc[i] = disps[i] * 6;

        }

        free(*su);
        free(*bp);
        free(*bh);
        free(*bl);
        free(*bw);
        free(*bn);
        free(*be);
        free(*bs);

        for (int i = 0; i < *nintcf + 1; i++ )
        {
            free((*lcc)[i]);
        }
        free(*lcc);

        *lcc = (int **) malloc((*nintcf + 1) * sizeof(int *));
        for (int i = 0; i < *nintcf + 1; i++ )
        {
            (*lcc)[i] = (int *) malloc(6 * sizeof(int));
        }


        *bs = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        *bn = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        *bw = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        *bl = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        *bh = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        *be = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        *bp = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);
        *su = (double *) calloc(sizeof(double), nElemsProc[myrank] + nExtElemsProc[myrank]);

        memcpy(*bs, bs_local, (nElemsProc[myrank] + nExtElemsProc[myrank])*sizeof(double));
        memcpy(*bn, bn_local, (nElemsProc[myrank] + nExtElemsProc[myrank])*sizeof(double));
        memcpy(*bw, bw_local, (nElemsProc[myrank] + nExtElemsProc[myrank])*sizeof(double));
        memcpy(*bl, bl_local, (nElemsProc[myrank] + nExtElemsProc[myrank])*sizeof(double));
        memcpy(*bh, bh_local, (nElemsProc[myrank] + nExtElemsProc[myrank])*sizeof(double));
        memcpy(*be, be_local, (nElemsProc[myrank] + nExtElemsProc[myrank])*sizeof(double));
        memcpy(*bp, bp_local, (nElemsProc[myrank] + nExtElemsProc[myrank])*sizeof(double));
        memcpy(*su, su_local, (nElemsProc[myrank] + nExtElemsProc[myrank])*sizeof(double));

        copy_lcc(nElemsProc[myrank], lcc_local, *lcc);
        *local_global_index = (int *) calloc(sizeof(int), nElemsProc[myrank]);

        for (int i = 0; i < nElemsProc[myrank]; ++i)
        {
            (*local_global_index)[i] = eMap[disps[myrank] + i];
        }

        free(bs_local);
        free(be_local);
        free(bn_local);
        free(bw_local);
        free(bl_local);
        free(bh_local);
        free(bp_local);
        free(su_local);
        free(lcc_local);

    }


    if ( f_status != 0 ) return f_status;


    *var = (double *) calloc(sizeof(double), (*nextcf + 1));
    *cgup = (double *) calloc(sizeof(double), (*nextcf + 1));
    *cnorm = (double *) calloc(sizeof(double), (*nintcf + 1));

    // initialize the arrays
    for (int i = 0; i <= 10; i++ )
    {
        (*cnorm)[i] = 1.0;
    }

    for (int i = (*nintci); i <= (*nintcf); i++ )
    {
        (*var)[i] = 0.0;
    }

    for (int i = (*nintci); i <= (*nintcf); i++ )
    {
        (*cgup)[i] = 1.0 / ((*bp)[i]);
        // (*cgup)[i] = 1.0;
    }

    for (int i = (*nextci); i <= (*nextcf); i++ )
    {
        (*var)[i] = 0.0;
        (*cgup)[i] = 0.0;
        (*bs)[i] = 0.0;
        (*be)[i] = 0.0;
        (*bn)[i] = 0.0;
        (*bw)[i] = 0.0;
        (*bh)[i] = 0.0;
        (*bl)[i] = 0.0;
    }

    /* ******************* COMMUNICATION MODEL ************************ */
    /* DUMMY INITIALIZATION - DELETE or ADJUST this by your convenience */
    MPI_Bcast( epart_int, num_tot_elems, MPI_INT, 0, MPI_COMM_WORLD );
    MPI_Bcast( &tot_exts, 1, MPI_INT, 0, MPI_COMM_WORLD );

    int curr_nghbs[nprocs];

    for (int i = 0; i < nprocs; ++i)
    {
        curr_nghbs[i] = -1;
    }

    for (int i = 0; i < *nintcf + 1; ++i)
    {
        for (int j = 0; j < 6; ++j)
        {
            if ((*lcc)[i][j] < num_tot_elems)
            {
                if (epart_int[(*lcc)[i][j]] != myrank)
                {
                    if (!already_in(epart_int[(*lcc)[i][j]], nprocs, curr_nghbs))
                    {
                        curr_nghbs[*nghb_cnt] = epart_int[(*lcc)[i][j]];
                        *nghb_cnt += 1;
                    }

                }
            }
        }
    }


    *nghb_to_rank = (int *) calloc(sizeof(int), *nghb_cnt);
    *send_cnt = (int *) calloc(sizeof(int), *nghb_cnt);
    *send_lst = (int **) calloc(sizeof(int *), *nghb_cnt);
    *recv_cnt = (int *) calloc(sizeof(int), *nghb_cnt);
    *recv_lst = (int **) calloc(sizeof(int *), *nghb_cnt);

    for (int i = 0; i < *nghb_cnt; ++i)
    {
        (*nghb_to_rank)[i] = curr_nghbs[i];
        (*send_lst)[i] = (int *) calloc(*nintcf + 1, sizeof(int));
        (*recv_lst)[i] = (int *) calloc(*nintcf + 1, sizeof(int));
    }

    for (int i = 0; i < *nghb_cnt; ++i)
    {
        for (int j = 0; j < *nintcf + 1; ++j)
        {
            (*send_lst)[i][j] = -1;
            (*recv_lst)[i][j] = -1;
        }
    }

    int idx_temp;
    for (int i = 0; i < *nintcf + 1; ++i)
    {
        for (int j = 0; j < 6; ++j)
        {
            if ((*lcc)[i][j] < num_tot_elems)
            {
                if (epart_int[(*lcc)[i][j]] != myrank)
                {
                    idx_temp = get_ngh_idx(epart_int[(*lcc)[i][j]], *nghb_cnt, *nghb_to_rank);

                    if (!already_in((*lcc)[i][j], (*recv_cnt)[idx_temp] , (*recv_lst)[idx_temp]))
                    {
                        (*recv_lst)[idx_temp][(*recv_cnt)[idx_temp]] = (*lcc)[i][j];
                        ((*recv_cnt)[idx_temp]) += 1;
                    }
                    if (!already_in((*local_global_index)[i], (*send_cnt)[idx_temp] , (*send_lst)[idx_temp]))
                    {
                        (*send_lst)[idx_temp][(*send_cnt)[idx_temp]] = (*local_global_index)[i];
                        ((*send_cnt)[idx_temp]) += 1;
                    }

                }
            }
        }
    }

    for (int i = 0; i < *nghb_cnt; ++i)
    {
        qsort((*send_lst)[i], (*send_cnt)[i], sizeof(int), cmpfunc);
        qsort((*recv_lst)[i], (*recv_cnt)[i], sizeof(int), cmpfunc);
    }

    *global_local_index = (int *) calloc(sizeof(int), num_tot_elems + tot_exts);

    for (int i = 0; i < nElemsProc[myrank]; ++i)
    {
        (*global_local_index)[(*local_global_index)[i]] = i;
    }

    int cnt = 0;
    for (int i = 0; i < *nghb_cnt; ++i)
    {
        for (int j = 0; j < (*recv_cnt)[i]; ++j)
        {
            (*global_local_index)[(*recv_lst)[i][j]] = *nextcf + 1 + cnt;
            cnt++;
        }
    }
    cnt = 0;
    for (int i = 0; i < nElemsProc[myrank]; ++i)
    {
        for (int j = 0; j < 6; ++j)
        {
            if ((*lcc)[i][j] >= num_tot_elems)
            {
                (*global_local_index)[(*lcc)[i][j]] = *nintcf + 1 + cnt;
                cnt++;
            }
        }
    }

    // test functions not needed and papi not supported on local machine

    // int input_key, part_key, read_key;
    // if (strcmp(part_type, "classic") == 0)
    //     part_key = 1;
    // else if (strcmp(part_type, "dual") == 0)
    //     part_key = 2;
    // else if (strcmp(part_type, "nodal") == 0)
    //     part_key = 3;
    // else
    //     part_key = 99;

    // if (strcmp(read_type, "oneread") == 0)
    //     read_key = 1;
    // else if (strcmp(read_type, "allread") == 0)
    //     read_key = 2;
    // else
    //     read_key = 99;


    // if (strcmp(file_in, "tjunc.geo.bin") == 0)
    //     input_key = 1;
    // else if (strcmp(file_in, "drall.geo.bin") == 0)
    //     input_key = 2;
    // else if (strcmp(file_in, "pent.geo.bin") == 0)
    //     input_key = 3;
    // else if (strcmp(file_in, "cojack.geo.bin") == 0)
    //     input_key = 4;
    // else
    //     input_key = 99;



    // end_vusec = PAPI_get_virt_usec();

    // final_vusec = end_vusec - start_vusec;

    // printf ( "Wall clock time in usecs: %lld\n", final_vusec );

    // write_pstats_exectime(input_key, part_key, read_key, myrank, final_vusec);
    // write_pstats_partition(input_key, part_key, myrank, nElemsProc[myrank], nExtElemsProc[myrank]);



    // double *scalars;


    // for (int i = 0; i < *nghb_cnt; ++i)
    // {
    //     printf("rank =%d send count[%d] = %d receive count[%d] %d \n", myrank, (*nghb_to_rank)[i], (*send_cnt)[i], (*nghb_to_rank)[i], (*recv_cnt)[i] );
    //write_pstats_communication(input_key, part_key, myrank, nprocs, *nghb_cnt, i, *send_cnt, *send_lst, *recv_cnt, *recv_lst );
    // char str[15];
    // char strNghb[15];
    // snprintf(str, 15, "rank%d.", myrank);
    // snprintf(strNghb, 15, "nghb%d.vtk", i);
    // strcat(str, strNghb);
    // printf("%s\n", str);
    // scalars = (double *) calloc(nElemsProc[myrank], sizeof(double));
    // for (int i = 0; i < nElemsProc[myrank]; ++i)
    // {
    //     scalars[i] = 1.0;
    // }
    //test_distribution(file_in, str, (*send_lst)[i], (*send_cnt)[i], scalars);
    //     free(scalars);
    // }

    free(disps);
    free(eMap);
    free(nElemsProc);
    free(disps_lcc);
    free(nElemsProc_lcc);
    free(epart_int);


    if (myrank == 0)
    {
        free(bs_new);
        free(be_new);
        free(bn_new);
        free(bw_new);
        free(bl_new);
        free(bh_new);
        free(bp_new);
        free(su_new);
        free(lcc_flat);
        for (int i = 0; i < num_tot_elems; i++ )
        {
            free(lcc_new[i]);
        }
        free(lcc_new);
        if (strcmp(part_type, "classic") != 0)
        {
            free(epart);
            free(eptr);
            free(eind);
            free(npart);
            free(objval);
            free(oNpart);
            free(nMap);
        }

    }


    return 0;
}

